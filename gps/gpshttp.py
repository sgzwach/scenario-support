#!/usr/bin/python

from bottle import route, run, template, static_file, request
import os
import subprocess
from pymongo import MongoClient
from bson import Binary, Code
from bson.objectid import ObjectId
from bson.json_util import dumps
import json
import datetime

PORT     = 8081
DATABASE = "gold_mongodb"
DBPORT   = 27017
USER     = "ship-admin"
PASSWORD = "ThisIsALongPassword123!!!"
RES_DIR  = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + 'resources' + os.path.sep

def parse_request(robj):
	data = {}
	data['hd'] = float(robj.forms.get('heading'))
	data['ap'] = robj.forms.get('autopilot') == "True"
	data['an'] = robj.forms.get('anchor') == "True"
	data['th'] = float(robj.forms.get('throttle'))
	data['dx'] = float(robj.forms.get('dx'))
	data['dy'] = float(robj.forms.get('dy'))
	data['al'] = int(robj.forms.get('alarms'))
	data['se'] = robj.forms.get('secret')
	return data


@route('/resources/<resource_path:path>')
def get_resource(resource_path):
	return static_file(resource_path, root=RES_DIR)

# @route('/')
# def main():
#     return get_resource('template.html')

@route('/gps/<ship_id>/', method='POST')
def get_tasking(ship_id):
	# get fakeGPS data
	r = parse_request(request)
	# print "Received request..."

	# login
	client = MongoClient(DATABASE, DBPORT)
	client.shipmgr.authenticate(USER, PASSWORD)
	db  = client.shipmgr
	col = db.ships

	# validate password/secret/key whatever
	es = col.find_one({"shipid": ship_id}, {"secret": 1, "_id": 0})
	# print "Does %s == %s" % (es, r['se'])
	if es['secret'] != r['se']:
		return {"result": 0}


	# update status based on info from ship
	col.update_one({"shipid": ship_id},
		{"$set": {
			'status.heading': r['hd'],
			'status.autopilot': r['ap'],
			'status.throttle': r['th'],
			'status.dx': r['dx'],
			'status.dy': r['dy'],
			'status.alarms': r['al'],
			'status.anchor': r['an'],
			'lastPLCCheckIn': datetime.datetime.now()
	}})

	# get status and return
	ship_task = col.find_one({"shipid": ship_id })
	# ship_id['status']['result'] = 0
	return dumps(ship_task['status'])

@route('/name/<ship_id>/', method='GET')
def get_name(ship_id):
	client = MongoClient(DATABASE, DBPORT)
	client.shipmgr.authenticate(USER, PASSWORD)
	db  = client.shipmgr
	col = db.ships

	r = col.find_one({"shipid": ship_id}, {"name": 1, "_id": 0})
	if r:
		return r['name']
	else:
		return "Unknown"

@route('/status/<ship_id>/', method='GET')
def get_status(ship_id):
	# this function lets us know the ship is going to be checking in
	# again and gets the initail state of the PLC from gold team.
	
	# login
	client = MongoClient(DATABASE, DBPORT)
	client.shipmgr.authenticate(USER, PASSWORD)
	db  = client.shipmgr
	col = db.ships

	ship_task = col.find_one({"shipid": ship_id})
	if ship_task:
		return dumps(ship_task['status'])
	else:
		return "{}"


run(host='0.0.0.0', port=PORT, server='cherrypy')

