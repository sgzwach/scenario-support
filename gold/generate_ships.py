#!/usr/bin/python

from random import choice
import string
from pymongo import MongoClient
import re

# read in shipnames
f = open('shipnames', 'r')
allships = f.read().split()
f.close()

# configure teams
teams = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

# authenticate to mongo
mc = MongoClient('gold_mongodb', 27017)
mc.shipmgr.authenticate('ship-admin', 'ThisIsALongPassword123!!!')
col = mc.shipmgr.ships

for t in teams:
	for i in xrange(1,6):
		ship = choice(allships)
		allships.remove(ship)
		print "Adding Team_%s_Ship_%d - %s" % (t, i, ship)

		# create document w/ dict
		doc = {
			"name": ship,
			"shipid": "t%ss%d" % (t, i),
			"secret": ''.join(choice(string.ascii_uppercase + string.digits) for _ in range(10)),
			"status": {
				"heading": 90,
				"autopilot": True,
				"throttle": 0,
				"dx": 50,
				"dy": 50,
				"alarms": 0,
				"x": 16.5,
				"y": 60,
				"fuel": 900,
				"anchor": False,
				"assigned": False
			},
			"task": {
				"assignedAt": None,
				"dueAt": None,
				"taskid": None,
				"remaining": 9000
			},
			"teamUpdated": False,
			"ttyport": str(8080+i),
			"lastPLCCheckIn": None
		}

		# insert into mongo
		col.insert_one(doc)

	# update team databases
	teams = mc.shipmgr.teams.find({})
	for t in teams:
		tmc = MongoClient(t['db'], int(t['port']))
		if len(t['user']):
			r = tmc.fleet.authenticate(t['user'], t['pass'])
		# clean it out
		tmc.fleet.ships.remove({})

		# copy it all over
		ships = mc.shipmgr.ships.find({'shipid': {'$regex': '^t' + re.escape(t['team'])}})
		for s in ships:
			tmc.fleet.ships.update({'shipid': s['shipid']}, {'$set': {'status': s['status'], 'name': s['name'], 'task': {'dx': 0, 'dy': 0}, 'ttyurl': 'http://' + t['db'] + ':' + str(s['ttyport'])}}, upsert=True)


