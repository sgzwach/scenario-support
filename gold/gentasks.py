#!/usr/bin/python

from pymongo import MongoClient
import random
import datetime

cargolist = ['cars', 'trucks', 'rubber ducks', 'apples', 'bananas', 'hats', 'cats', 'dogs', 'vaccines']
customers = ['PCDC', 'Europe Distribution Corp', 'Online Sales International', 'Scott Bell']
dests     = [(79.5, 21), (76, 51)]
teams     = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

def getCustomerCargo():
	cc = {}
	for i in xrange(random.randrange(2,6)):
		cc[random.choice(cargolist)] = random.randint(500,150000)
	return cc

# connect to database
mc = MongoClient('gold_mongodb', 27017)
mc.shipmgr.authenticate('ship-admin', 'ThisIsALongPassword123!!!')
col = mc.shipmgr.tasks

# clear old tasks
col.remove({})

# update all ships
mc.shipmgr.ships.update({}, {'$set': {'status.assigned': False}}, multi=True)

t = datetime.datetime.now() + datetime.timedelta(minutes=1)

# generate a manifest every 4-5 minutes
for i in xrange(100):
	# get unique customers
	cl = random.sample(customers, random.randint(1,4))
	mf = {}
	for c in cl:
		mf[c] = getCustomerCargo()

	d = random.choice(dests)
	# assign to each team
	for tid in teams:
		secondmodifier = datetime.timedelta(seconds=random.randrange(1,30))
		col.insert({'teamid': tid, 'cargo': mf, 'updatedAt': datetime.datetime.now(), 'assignAt': t + secondmodifier, 'dueAt': t + datetime.timedelta(minutes=15) + secondmodifier, "dx": d[0], "dy": d[1], 'assigned': False, 'status': {'complete': False, 'valid-manifest': False}})

	# adjust time for next iteration
	t = t + datetime.timedelta(minutes=5)




