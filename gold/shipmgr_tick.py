#!/usr/bin/python

from pymongo import MongoClient
from bson.objectid import ObjectId
from time import sleep
import random
import sys
import numpy
import os
import pprint
import datetime
import re

# every second, get all ships and update those that are assigned to a task
USER = "ship-admin"
PASS = "ThisIsALongPassword123!!!"
HOST = 'gold_mongodb'
PORT = 27017

mc = MongoClient(HOST, PORT)
if not mc.shipmgr.authenticate(USER, PASS):
	print "ERROR: Unable to authenticate with creds provided"
	sys.exit(1)

db = mc.shipmgr
pp = pprint.PrettyPrinter(indent=4)

def completeTask(tid, sid, success):
	# pull manifest from team and task
	ms = False
	task_manifest = db.tasks.find_one({'_id': ObjectId(tid)}, {'_id': 0, 'cargo': 1, 'teamid': 1})
	try:
		teamdb = db.teams.find_one({'team': task_manifest['teamid']})
		tmc = MongoClient(teamdb['db'], int(teamdb['port']))
		if len(teamdb['user']):
			tmc.fleet.authenticate(teamdb['user'], teamdb['pass'])
		team_manifest = tmc.fleet.ships.find_one({'shipid': sid}, {'_id': 0, 'manifest': 1})
		if team_manifest['manifest'] == task_manifest['cargo']:
			ms = True
	except:
		pass

	# mark task complete
	db.tasks.update({'_id': ObjectId(tid)}, {'$set': {
			'updatedAt': datetime.datetime.now(),
			'status.complete': True,
			'status.success': success,
			'status.valid-manifest': ms
	}})

	# mark ship unassigned, move to home port, dump fuel
	db.ships.update({'shipid': sid}, {'$set': {
			'status.y': 60,
			'status.assigned': False,
			'status.fuel': 900,
			'status.dx': 50,
			'status.dy': 50,
			'status.throttle': 0,
			'status.autopilot': True,
			'status.anchor': False,
			'status.alarms': 0,
			'status.x': 16.5,
			'status.heading': 0,
			'task.dueAt': None,
			'task.assignedAt': None,
			'task.taskid': None,
			'task.remaining': 9000
	}})

	print 'Task', tid, 'completed!'

# clean up old trash once
# db.tasks.remove({'$and': [{'assigned': False}, {'assignAt': {'$lt': datetime.datetime.now()}}]})

while 1:
	sleep(1)

	# if a ship has a task assigned move it along
	# mark complete if necessary
	assigned = db.ships.find({'status.assigned': True})
	for d in assigned:
		did = d['shipid']
		cx = d['status']['x']
		cy = d['status']['y']
		dx = d['status']['dx']
		dy = d['status']['dy']
		rem = d['task']['remaining']
		tid = d['task']['taskid']
		thr = d['status']['throttle']
		lpc = d['lastPLCCheckIn']
		nx = 0
		ny = 0

		task_data = db.tasks.find_one({'_id': ObjectId(tid)})
		
		# if ship hasn't checked in recently, gameover
		if (lpc and (lpc + datetime.timedelta(minutes=1)) < datetime.datetime.now()):
			completeTask(tid, did, False)
			continue

		if abs(cx-task_data['dx']) <= 3 and abs(cy-task_data['dy']) <= 3:
			completeTask(tid, did, True)
			continue

		# are we late?
		if task_data['dueAt'] <= datetime.datetime.now():
			completeTask(tid, did, False)
			continue

		# if we're out of fuel we can't 'go forward'
		if d['status']['fuel'] <= 0:
			completeTask(tid, did, False)
			continue

		# if no throttle, just carry on
		if thr <= 0:
			continue

		# if autopilot is enabled, update using simple math and rock on
		if d['status']['autopilot']:
			# (nx, ny) == updated gps location
			nx = (dx - cx) / (rem / thr) + cx
			ny = (dy - cy) / (rem / thr) + cy

		# if autopilot isn't enabled, calculate new location given heading
		else:
			# determine how far we're going
			dis = (thr / 120)

			# convert heading to usable angle
			hd = d['status']['heading']
			hd += 270 
			hd = numpy.deg2rad(hd)
			nx = cx + (numpy.cos(hd) * dis)
			ny = cy + (numpy.sin(hd) * dis)
			# if hd <= 90 and hd >= 0: # top right
			# 	dg = 90 - hd
			# 	ny = cy + (numpy.sin(dg) * dis * -1)
			# 	nx = cx + (numpy.cos(dg) * dis)
			# elif hd > 90 and hd <= 180: # bottom right
			# 	dg = hd - 90
			# 	ny = cy + (numpy.sin(dg) * dis)
			# 	nx = cx + (numpy.cos(dg) * dis)
			# elif hd > 180 and hd <= 270: # bottom left
			# 	dg = hd - 180
			# 	ny = cy + (numpy.cos(dg) * dis)
			# 	nx = cx + (numpy.sin(dg) * dis * -1)
			# else: # top left
			# 	dg = hd - 270
			# 	ny = cy + (numpy.sin(dg) * dis * -1)
			# 	nx = cx + (numpy.sin(dg) * dis * -1)

		# validate and update location
		if nx < 0:
			nx = 0
		elif nx > 100:
			nx = 100
		if ny < 0:
			ny = 0
		elif ny > 100:
			ny = 100
		db.ships.update({"shipid": did}, {"$set": 
			{
				"status.x": nx,
				"status.y": ny
			}})

		# update fuel level
		if thr <= 15 and thr > 0:
			fc = 750 / (9000 / thr)
		else:
			fc = (750 / (9000 / thr)) * (1 + ((thr - 15) * .04))
		nf = d['status']['fuel'] - fc
		if nf <= 0:
			nf = 0
		db.ships.update({"shipid": did}, {"$set": {"status.fuel": nf}})

		# update remaining "distance" (length of AB * 120)
		rem = numpy.sqrt((nx-dx)**2 + (ny-dy)**2) * 120
		db.ships.update({"shipid": did}, {"$set": {"task.remaining": rem}})

	# assign anything we ought to
	unassigned = db.tasks.find({'$and': [{'assigned': False}, {'assignAt': {'$lt': datetime.datetime.now()}}]})
	for t in unassigned:
		# get a 'random' ship
		ships = db.ships.find({'$and': [{'shipid': {'$regex': '^t' + re.escape(t['teamid'])}}, {'status.assigned': False}]})
		sl = []
		for s in ships:
			sl.append(s['shipid'])
		if not len(sl):
			continue
		ship = db.ships.find_one({'shipid': random.choice(sl)})
		print "assigning", t['_id'], 'to', ship['shipid']

		# mark task assigned
		db.tasks.update({'_id': ObjectId(t['_id'])}, {'$set': {
				'assigned': True,
				'updatedAt': datetime.datetime.now(),
				'assignedTo': ship['shipid']
		}})

		# update shipmgr status info
		db.ships.update({'shipid': ship['shipid']}, {'$set': {
			'task.dueAt': t['dueAt'],
			'task.taskid': ObjectId(t['_id']),
			'status.assigned': True,
			'status.fuel': 850,
			'status.dx': t['dx'],
			'status.dy': t['dy'],
			'status.throttle': 15,
			'status.x': 16.5,
			'status.y': 60,
			'status.autopilot': True,
			'status.anchor': False,
			'teamUpdated': False
		}})
		
	# update ships for all teams on their mongo server
	teams = db.teams.find()
	for t in teams:
		# connect to team
		tmc = MongoClient(t['db'], int(t['port']))
		# authenticate if we have to
		if len(t['user']) > 0:
			r = tmc.fleet.authenticate(t['user'], t['pass'])
			if not r:
				print "unable to access team", t['team']
				continue
		# update ships that changed since last time
		ships = db.ships.find({'$and': [{'shipid': {'$regex': '^t' + re.escape(t['team'])}}, {'teamUpdated': False}, {'status.assigned': True}]})
		for s in ships:
			# fetch manifest
			manifest = db.tasks.find_one({'_id': ObjectId(s['task']['taskid'])})
			tmc.fleet.ships.update({'shipid': s['shipid']}, {'$set': {
				'manifest': manifest['cargo'],
				'task.dx': manifest['dx'],
				'task.dy': manifest['dy']
			}}, upsert=True)
			db.ships.update({'shipid': s['shipid']}, {'$set': {'teamUpdated': True}})
			
		# update the rest of the ship statuses
		ships = db.ships.find({'shipid': {'$regex': '^t' + re.escape(t['team'])}})
		for s in ships:
			tmc.fleet.ships.update({'shipid': s['shipid']}, {'$set': {
				'status': s['status'],
				'name': s['name'],
				'ttyurl': 'http://' + t['db'] + ':' + str(s['ttyport'])
			}}, upsert=True)
