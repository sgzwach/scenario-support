#!/usr/bin/python

from pymongo import MongoClient
import datetime

# authenticate to mongo
mc = MongoClient('gold_mongodb', 27017)
mc.shipmgr.authenticate('ship-admin', 'ThisIsALongPassword123!!!')
col = mc.shipmgr.ships

# reset all ship locations, statuses, etc.
col.update({}, {'$set': {
		'status.x': 60,
		'status.assigned': False,
		'status.fuel': 850,
		'status.dx': 50,
		'status.dy': 50,
		'status.throttle': 0,
		'status.autopilot': True,
		'status.anchor': False,
		'status.alarms': 0,
		'status.x': 16.5,
		'status.heading': 90,
		'task.dueAt': None,
		'task.assignedAt': None,
		'task.taskid': None,
		'task.remaining': 9000,
		'teamUpdated': False,
		'lastPLCCheckIn': datetime.datetime.now()
}}, multi=True) 

# do the same for teams
teams = mc.shipmgr.teams.find({})
for t in teams:
	tmc = MongoClient(t['db'], int(t['port']))
	if len(t['user']):
		r = tmc.fleet.authenticate(t['user'], t['pass'])
	# reset ship locations, statues, etc.
	tmc.fleet.ships.update({}, {'$set': {
		'status.throttle': 0,
		'status.alarms': 0,
		'status.autopilot': True,
		'status.heading': 90,
		'status.assigned': False,
		'status.fuel': 850,
		'status.dx': 50,
		'status.dy': 50,
		'status.y': 60,
		'status.x': 16.5,
		'status.anchor': False,
		'task.dx': 0,
		'task.dy': 0
	}}, multi=True)


