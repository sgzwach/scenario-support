#!/usr/bin/python

from pymodbus.client.sync import ModbusTcpClient as ModbusClient
import random

mc = ModbusClient('127.0.0.1', port=5020)
mc.connect()

while 1:
	mc.write_register(0x2, random.randrange(100))
	mc.write_register(0x3, random.randrange(100))
