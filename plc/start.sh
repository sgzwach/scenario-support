#!/bin/bash
cd /root/app
set -m
(
  ( ./shipplc.py 5020 $1 $2 $3 $4 > /dev/null 2>&1|| kill 0 ) &
  ( nodejs wetty/app.js -p 8080 -h $1 -o $2 -i $3 > /dev/null 2>&1|| kill 0 ) 
)
