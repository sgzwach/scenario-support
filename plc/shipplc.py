#!/usr/bin/python

###############################################
# FakeShipPLC for PCDC
###############################################
###############################################
# Imports               
###############################################

from pymodbus.server.async import StartTcpServer
from pymodbus.server.async import StartUdpServer
from pymodbus.server.async import StartSerialServer

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer

from pymodbus.client.sync import ModbusTcpClient as ModbusClient

import logging
import sys
from time import sleep
from threading import Thread
import os
import numpy
import requests

###############################################
# Gold Team Jazz
###############################################
GT_GPS_ADDRESS = '127.0.0.1'
GT_GPS_PORT    = 8081
GT_SHIPID      = ""# DO NOT change this. It will only hurt you.
GT_SECRET      = ""

###############################################
# Logging             
###############################################

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.INFO)

###############################################
# Holding Registers (rw) 2bytes
###############################################

PLC_HR_HEADING      = 0x1   # Current heading; can only change w/ autopilot off; degrees 
PLC_HR_DESTX        = 0x2   # X coordinate (%) of destination
PLC_HR_DESTY        = 0x3   # Y coordinate (%) of destination
PLC_HR_THROTTLE     = 0x4   # 0-15 (can go greater, limited by software @ 25)

###############################################
# Input Registers (ro) 2bytes
###############################################

PLC_IR_X     = 0x1
PLC_IR_Y     = 0x2
PLC_IR_FUEL  = 0x3  # how many tons of fuel remain
PLC_IR_SPEED = 0x4  # ~(throttle/15)*25

###############################################
# Discrete Inputs (ro) 1bit
###############################################

PLC_DI_UNDERWAY     = 0x1
PLC_DI_ANCHORWARN   = 0x2   # alert if underway && anchor
PLC_DI_HEADINGWARN  = 0x3   # alert if heading invalid for dest 
PLC_DI_THROTTLEWARN = 0x4   # alert if throttle > 15 (25 knots)
PLC_DI_GPSWARN      = 0x5   # alert if GPS signal is unavailable (critical fail)
PLC_DI_FUELWARN     = 0x6   # alert if fuel level below 300 tons

###############################################
# Coils (rw) 1bit
###############################################

PLC_CO_ANCHOR    = 0x1
PLC_CO_AUTOPILOT = 0x2

###############################################
# Datastores
###############################################

store = ModbusSlaveContext(
	di = ModbusSequentialDataBlock(0, [False]*100),
	co = ModbusSequentialDataBlock(0, [False]*100),
	hr = ModbusSequentialDataBlock(0, [0]*100),
	ir = ModbusSequentialDataBlock(0, [0]*100))
context = ModbusServerContext(slaves=store, single=True)

###############################################
# Server Information
###############################################

identity = ModbusDeviceIdentification()
identity.VendorName  = "Pymodbus"
identity.ProductCode = "FSP"
identity.VendorUrl   = "http://github.com/bashwork/pymodbus/"
identity.ProductName = "Pymodbus Server"
identity.ModelName   = "FakeShipPLC"
identity.MajorMinorRevision = "0.1"

##############################################
# Ship logic (it's pretty rough...)
##############################################
# retrieve discrete input data
def PLCGetDI(addr):
	return context[0x0].getValues(2, addr, count=1)[0]

# set discrete input data
def PLCSetDI(addr, value):
	return context[0x0].setValues(2, addr, [value])

# get coil data
def PLCGetCO(addr):
	return context[0x0].getValues(1, addr, count=1)[0]

# set coil data
def PLCSetCO(addr, value):
	return context[0x0].setValues(1, addr, [value])

# retrieve holding register data
def PLCGetHR(addr):
	return context[0x0].getValues(3, addr, count=1)[0]

# set holding register data
def PLCSetHR(addr, value):
	return context[0x0].setValues(3, addr, [value])

# retrieve input register data
def PLCGetIR(addr):
	return context[0x0].getValues(4, addr, count=1)[0]

# retrieve input register data
def PLCSetIR(addr, value):
	return context[0x0].setValues(4, addr, [value])

# emulate ship logic stuff using magic...
def emulate():
	# debug print status
	os.system("clear")
	print "DI:", context[0x0].getValues(2, 1, count=6)
	print "CO:", context[0x0].getValues(1, 1, count=2)
	print "HR:", context[0x0].getValues(3, 1, count=4)
	print "IR:", context[0x0].getValues(4, 1, count=4)

	# check anchor status
	if (PLCGetCO(PLC_CO_ANCHOR) == True):
		PLCSetHR(PLC_HR_THROTTLE, 0)
		PLCSetDI(PLC_DI_ANCHORWARN, True)
	else:
		PLCSetDI(PLC_DI_ANCHORWARN, False)

	# Check throttle for overworking
	if (PLCGetHR(PLC_HR_THROTTLE) > 15):
		PLCSetDI(PLC_DI_THROTTLEWARN, True)
	else:
		PLCSetDI(PLC_DI_THROTTLEWARN, False)

	# Check if fuel level is low
	if (PLCGetIR(PLC_IR_FUEL) < 200):
		PLCSetDI(PLC_DI_FUELWARN, True)
	else:
		PLCSetDI(PLC_DI_FUELWARN, False)

	# adjust throttle if outside specs
	if PLCGetHR(PLC_HR_THROTTLE) > 25:
		PLCSetHR(PLC_HR_THROTTLE, 25)

	# validate/set heading (yikes)
	x = PLCGetIR(PLC_IR_X)
	y = PLCGetIR(PLC_IR_Y)
	dx = PLCGetHR(PLC_HR_DESTX)
	dy = PLCGetHR(PLC_HR_DESTY)
	dg = numpy.arctan2(dy-y, dx-x)
	dg = numpy.rad2deg(dg)
	
	# convert to cartesian deg where 0 is N
	if dg < 0 and dg >= -90: # top right
		hd = 90 + dg
	elif dg < -90: # top left
		hd = dg + 90 + 360
	else: # bottom left or right
		hd = 90 + dg

	# if dg <= 90 and dg >= 0:
	# 	hd = 90 - dg
	# elif dg > 90 and dg <= 180:
	# 	hd = 90 - (dg - 90) + 270
	# else:
	# 	hd = abs(dg) + 90

	# who needs precision
	# HMI does NOT receive precision...
	hd = float("{0:.2f}".format(hd))
	if PLCGetCO(PLC_CO_AUTOPILOT):
		PLCSetHR(PLC_HR_HEADING, hd)
	
	# validate heading
	ah = PLCGetHR(PLC_HR_HEADING)
	if ah >= 360:
		PLCSetHR(PLC_HR_HEADING, ah % 360)
	diff = abs(ah - hd)
	if diff <= 1:
		PLCSetDI(PLC_DI_HEADINGWARN, False)
	else:
		PLCSetDI(PLC_DI_HEADINGWARN, True)

	# fake speed
	PLCSetIR(PLC_IR_SPEED, PLCGetHR(PLC_HR_THROTTLE)*1.2)

	# get (fake)GPS, fuel, etc. from Gold team
	# update our register values as noted with them as well
	dt = {}
	dt['throttle']  = PLCGetHR(PLC_HR_THROTTLE)
	dt['dx']        = PLCGetHR(PLC_HR_DESTX)
	dt['dy']        = PLCGetHR(PLC_HR_DESTY)
	dt['heading']   = PLCGetHR(PLC_HR_HEADING)
	dt['autopilot'] = PLCGetCO(PLC_CO_AUTOPILOT)
	dt['anchor']    = PLCGetCO(PLC_CO_ANCHOR)
	dt['secret']    = GT_SECRET

	# ignore fuel, x, y, and assigned as those are controlled by ShipManager
	# count alarms
	alarms = 0
	for i in xrange(2, PLC_DI_FUELWARN+1):
		alarms += 1 if PLCGetDI(i) else 0
	alarms += 1 if not PLCGetCO(PLC_CO_AUTOPILOT) else 0	
	dt['alarms']    = alarms

	# only post status if assigned, else retrieve status to 'reset' ship
	if PLCGetDI(PLC_DI_UNDERWAY):
		try:
			r = requests.post('http://' + GT_GPS_ADDRESS + ':' + str(GT_GPS_PORT) + '/gps/' + GT_SHIPID + '/', data=dt, timeout=1)
			if r.status_code == 200:
				PLCSetDI(PLC_DI_GPSWARN, False)
				print r.text
				n = r.json()
				PLCSetIR(PLC_IR_FUEL, n['fuel'])
				PLCSetIR(PLC_IR_X, n['x'])
				PLCSetIR(PLC_IR_Y, n['y'])
				PLCSetDI(PLC_DI_UNDERWAY, n['assigned'])
			else:
				PLCSetDI(PLC_DI_GPSWARN, True)
		except:
			PLCSetDI(PLC_DI_GPSWARN, True)
	else:
		fetch_status()

# conatct gold team to intialze registers since we're all volitile up in this ship
# on fail exit entire mess
def fetch_status():
	print "Connecting to %s:%d using secret %s" % (GT_GPS_ADDRESS, GT_GPS_PORT, GT_SECRET)
	try:
		r = requests.get('http://' + GT_GPS_ADDRESS + ':' + str(GT_GPS_PORT) + '/status/' + GT_SHIPID + '/', timeout=5)
		print r.text
		if r.status_code == 200:
			# if we get the deets, configure EVERY register/coil/input :'(
			n = r.json()
			PLCSetHR(PLC_HR_THROTTLE, n['throttle'])
			PLCSetHR(PLC_HR_DESTX, n['dx'])
			PLCSetHR(PLC_HR_DESTY, n['dy'])
			PLCSetHR(PLC_HR_HEADING, n['heading'])
			PLCSetIR(PLC_IR_X, n['x'])
			PLCSetIR(PLC_IR_Y, n['y'])
			PLCSetIR(PLC_IR_FUEL, n['fuel'])
			PLCSetDI(PLC_DI_UNDERWAY, n['assigned'])
			# skip warning DI's as they'll be immediately calculated 
			PLCSetCO(PLC_CO_AUTOPILOT, n['autopilot'])
			PLCSetCO(PLC_CO_ANCHOR, n['anchor'])
			return True
		else:
			# fail
			return False
	except:
		# moar fail
		return False

# do emulate every second or so	
# should actually make async eventually
def emulate_loop():
	# get status; if fails -> try to quit
	r = fetch_status()
	c = 1
	while not r and c < 10:
		r = fetch_status()
		c += 1
	if not r:
		sys.exit(1)

	# carry on forever
	while 1:
		emulate()
		sleep(1)

##############################################
# Start Server 
##############################################

# gather configuration information from command line
if len(sys.argv) < 6:
	print "ERROR: need cmd args to know what to start"
	print "./shipplc.py <plc_port> <gold_host> <gold_port> <shipid> <secret>"
	sys.exit(1)

GT_GPS_ADDRESS = sys.argv[2]
GT_GPS_PORT    = int(sys.argv[3])
GT_SHIPID      = sys.argv[4]
GT_SECRET      = sys.argv[5]

# emulate ship status logic
thread = Thread(target=emulate_loop)
thread.daemon = True
thread.start()

# start modbustcp server
StartTcpServer(context, identity=identity, address=("localhost", int(sys.argv[1]))) 

