#!/usr/bin/python

from bottle import route, run, template, static_file
import os
import sys
import subprocess
from pymongo import MongoClient
from bson import Binary, Code
from bson.json_util import dumps
import ConfigParser

PORT     = 8080
DATABASE = "127.0.0.1"
DBPORT   = 27017
USER     = ""
PASSWORD = ""
RES_DIR  = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + 'resources' + os.path.sep

@route('/resources/<resource_path:path>')
def get_resource(resource_path):
    #print('[INFO] - Getting file ' + resource_path)
    return static_file(resource_path, root=RES_DIR)

@route('/')
def main():
    return get_resource('template.html')


@route('/ships/')
def get_ships():
    # rebase to use mongo w/ ships
    client = MongoClient(DATABASE, DBPORT)

    # login if we must
    if len(USER):
        client.shipmgr.authenticate(USER, PASSWORD)
    db = client.shipmgr
    col = db.ships
    ships = col.find({}, {'_id': 0, 'name': 1, 'shipid': 1, 'status.assigned': 1, 'status.alarms': 1, 'status.x': 1, 'status.y': 1})
    
    return dumps(ships)

@route('/score/')
def get_score():
    client = MongoClient(DATABASE, DBPORT)
    if len(USER):
        client.shipmgr.authenticate(USER, PASSWORD)
    
    # count the things carefully
    pipeline = [
        {"$match": {"assigned": True}},
        {"$project": {
            "_id": 0,
            "teamid": 1,
            "sc": {"$cond": [{"$eq": ["$status.success", True]}, 1, 0]},
            "mc": {"$cond": [{"$eq": ["$status.valid-manifest", True]}, 1, 0]},
        }},
        {"$group": {
            "_id": {"team": "$teamid"},
            "SumAS": {"$sum": 1},
            "SumSC": {"$sum": "$sc"},
            "SumMC": {"$sum": "$mc"}
        }},
        {"$sort": {"_id.team": 1}}
    ]
    return dumps(client.shipmgr.tasks.aggregate(pipeline))

# load config
c = ConfigParser.SafeConfigParser()
c.readfp(open('srv.conf'))
try:
    # webserver section
    PORT=c.get('WebServer', 'port')

    # fakeGPS section
    GTGPS=c.get('ShipSoftGold', 'host')
    GTPRT=c.getint('ShipSoftGold', 'port')

    # mongo section
    DATABASE = c.get('Database', 'host')
    DBPORT = c.getint('Database', 'port')
    USER = c.get('Database', 'user')
    PASSWORD = c.get('Database', 'pass')

except:
    print "Unable to load config! Need server info."
    sys.exit(1)

run(host='0.0.0.0', port=PORT, server='cherrypy')


