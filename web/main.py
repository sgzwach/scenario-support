#!/usr/bin/python

from bottle import route, run, template, static_file
import os
import sys
import subprocess
from pymongo import MongoClient
from bson import Binary, Code
from bson.json_util import dumps
import ConfigParser

PORT     = 8080
DATABASE = "127.0.0.1"
DBPORT   = 27017
USER     = ""
PASSWORD = ""
RES_DIR  = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + 'resources' + os.path.sep

@route('/resources/<resource_path:path>')
def get_resource(resource_path):
    #print('[INFO] - Getting file ' + resource_path)
    return static_file(resource_path, root=RES_DIR)

@route('/')
def main():
    return get_resource('template.html')

@route('/dashboard/')
def get_dashboard():
    return get_resource('quad.html')
	
# Don't forget to remove before production, or that would be bad :)
@route('/cmd/<command>')
@route('/cmd/<command:path>')
def get_command(command):
    return subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()

@route('/ships/')
def get_ships():
    global x
    global y

    # rebase to use mongo w/ ships
    client = MongoClient(DATABASE, DBPORT)

    # login if we must
    if len(USER):
        client.fleet.authenticate(USER, PASSWORD)
    db = client.fleet
    col = db.ships
    ships = col.find()
    
    return dumps(ships)

@route('/manifest/<ship_id>/')
def get_manifest(ship_id):
    client = MongoClient(DATABASE, DBPORT)
    if len(USER):
        client.fleet.authenticate(USER, PASSWORD)
    manifest = client.fleet.ships.find_one({'shipid': ship_id})
    if manifest:
        return dumps(manifest)
    else:
        return "{}"

# load config
c = ConfigParser.SafeConfigParser()
c.readfp(open('srv.conf'))
try:
    # webserver section
    PORT=c.get('WebServer', 'port')

    # fakeGPS section
    GTGPS=c.get('ShipSoftGold', 'host')
    GTPRT=c.getint('ShipSoftGold', 'port')

    # mongo section
    DATABASE = c.get('Database', 'host')
    DBPORT = c.getint('Database', 'port')
    USER = c.get('Database', 'user')
    PASSWORD = c.get('Database', 'pass')

except:
    print "Unable to load config! Need server info."
    sys.exit(1)

run(host='0.0.0.0', port=PORT, server='cherrypy')


