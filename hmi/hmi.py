#!/usr/bin/python

import curses, sys
import subprocess
import curses.textpad
from curses import wrapper
from time import sleep
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from hashlib import md5
import requests

PLC_IP     = '127.0.0.1'      # Where is the PLC
PLC_PORT   = 5020             # What port is is the PLC listening on
PLC_NAME   = "Maersk Alabama"   # What's the name of the ship
PLC_CLIENT = None             # holds modbusclient once initiated
FLASH      = True             # stores color state for flashing txt

###############################################
# Holding Registers (rw) 2bytes
###############################################

PLC_HR_HEADING      = 0x1   # Current heading; can only change w/ autopilot off; degrees 
PLC_HR_DESTX        = 0x2   # X coordinate (%) of destination
PLC_HR_DESTY        = 0x3   # Y coordinate (%) of destination
PLC_HR_THROTTLE     = 0x4   # 0-15 (can go greater, limited by software @ 25)

###############################################
# Input Registers (ro) 2bytes
###############################################

PLC_IR_X     = 0x1
PLC_IR_Y     = 0x2
PLC_IR_FUEL  = 0x3  # how many tons of fuel remain
PLC_IR_SPEED = 0x4  # ~(throttle/15)*25

###############################################
# Discrete Inputs (ro) 1bit
###############################################

PLC_DI_UNDERWAY     = 0x1
PLC_DI_ANCHORWARN   = 0x2   # alert if underway && anchor
PLC_DI_HEADINGWARN  = 0x3   # alert if heading invalid for dest 
PLC_DI_THROTTLEWARN = 0x4   # alert if throttle > 15 (25 knots)
PLC_DI_GPSWARN      = 0x5   # alert if GPS signal is unavailable (critical fail)
PLC_DI_FUELWARN     = 0x6   # alert if fuel level below 300 tons

###############################################
# Coils (rw) 1bit
###############################################

PLC_CO_ANCHOR    = 0x1
PLC_CO_AUTOPILOT = 0x2

def fetch_registers():
	global PLC_CLIENT
	status = {}
	status['hr'] = PLC_CLIENT.read_holding_registers(0,5).registers
	status['ir'] = PLC_CLIENT.read_input_registers(0,5).registers
	status['di'] = PLC_CLIENT.read_discrete_inputs(0,7).bits
	status['co'] = PLC_CLIENT.read_coils(0,3).bits
	return status

def read_register(t, n):
	if t == 'hr':
		return PLC_CLIENT.read_holding_registers(n, 1).registers[0]
	elif t == 'ir':
		return PLC_CLIENT.read_input_registers(n, 1).registers[0]
	elif t == 'di':
		return PLC_CLIENT.read_discrete_inputs(n, 1).bits[0]
	else:
		return PLC_CLIENT.read_coils(n, 1).bits[0]

def write_register(t, n, val):
	if t == 'hr':
		PLC_CLIENT.write_register(n, val)
	else:
		PLC_CLIENT.write_coil(n, val)

def update_status(stdscr, y, x):
	# flip the bit for red text
	global FLASH
	FLASH = not FLASH
	r = fetch_registers()
	nor = curses.color_pair(1)
	red = curses.color_pair(4)

	# Update autopilot status
	ap = r['co'][PLC_CO_AUTOPILOT]
	stdscr.addstr(y+1, x+33, "ON  " if ap else "OFF ", red if (not ap and FLASH) else nor)

	# Update heading
	hd = r['hr'][PLC_HR_HEADING]
	stdscr.addstr(y+3, x+11, "%3d" % (hd, ))
	
	# update throttle
	th = r['hr'][PLC_HR_THROTTLE]
	stdscr.addstr(y+3, x+32, "%3d" % (th, ))

	# update speed
	sd = r['ir'][PLC_IR_SPEED]
	stdscr.addstr(y+4, x+11, "%3d" % (sd,))

	# update fuel level
	fl = r['ir'][PLC_IR_FUEL]
	stdscr.addstr(y+4, x+32, "%3d" % (fl, ))

	# are we underway?
	uw = r['di'][PLC_DI_UNDERWAY]
	stdscr.addstr(y+5, x+11, "YES" if uw else "NO ")

	# is the anchor down?
	ad = r['co'][PLC_CO_ANCHOR]
	stdscr.addstr(y+5, x+32, "DOWN" if ad else "UP  ")

	# where in the world is carmen sandiago
	cloc = '(%.0f, %.0f)' % (r['ir'][PLC_IR_X], r['ir'][PLC_IR_Y])
	stdscr.addstr(y+6, x+11, cloc)

	# are we there yet?
	floc = '(%.0f, %.0f)' % (r['hr'][PLC_HR_DESTX], r['hr'][PLC_HR_DESTY])
	stdscr.addstr(y+7, x+11, floc)

	# Check all alarms and update
	ta = r['di'][PLC_DI_HEADINGWARN]
	stdscr.addstr(y+11, x+13, "ALRM" if ta else "OK  ", red if (ta and FLASH) else nor )
	ta = r['di'][PLC_DI_THROTTLEWARN]
	stdscr.addstr(y+11, x+33, "ALRM" if ta else "OK  ", red if (ta and FLASH) else nor )
	ta = r['di'][PLC_DI_GPSWARN]
	stdscr.addstr(y+12, x+13, "ALRM" if ta else "OK  ", red if (ta and FLASH) else nor )
	ta = r['di'][PLC_DI_FUELWARN]
	stdscr.addstr(y+12, x+33, "ALRM" if ta else "OK  ", red if (ta and FLASH) else nor )
	ta = r['di'][PLC_DI_ANCHORWARN]
	stdscr.addstr(y+13, x+13, "ALRM" if ta else "OK  ", red if (ta and FLASH) else nor )

def draw_status(stdscr, y, x):
	stdscr.addstr(y,x,    "|---------------------------------------|")
	stdscr.addstr(y+1,x,  "| Ship Status        | Autopilot OFF    |")
	stdscr.addstr(y+2,x,  "|---------------------------------------|")
	stdscr.addstr(y+3,x,  "|  Heading      deg  | Throttle         |")
	stdscr.addstr(y+4,x,  "|    Speed      kts  |     Fuel     tons|")
	stdscr.addstr(y+5,x,  "| Assigned           |   Anchor         |")
	stdscr.addstr(y+6,x,  "| Location                              |")
	stdscr.addstr(y+7,x,  "|     Dest                              |")
	stdscr.addstr(y+8,x,  "|---------------------------------------|")
	stdscr.addstr(y+9,x,  "| Alarms                                |")
	stdscr.addstr(y+10,x, "|---------------------------------------|")
	stdscr.addstr(y+11,x, "|  Heading |        | Throttle |        |")
	stdscr.addstr(y+12,x, "|  GPSConn |        |  FuelLvl |        |")
	stdscr.addstr(y+13,x, "|   Anchor |        |                   |")
	stdscr.addstr(y+14,x, "|---------------------------------------|")

def draw_main(stdscr):
	stdscr.clear()
	curses.curs_set(False)

	# setup ship info
	stdscr.bkgd(0, curses.color_pair(1))
	stdscr.addstr(3,8,  "|---------------------------------------|")
	stdscr.addstr(4,8,  "| ShipManager v1.1 |                    |")
	stdscr.addstr(5,8,  "|---------------------------------------|")
	stdscr.addstr(6,8,  "| Ship:                                 |")
	stdscr.addstr(6,16, PLC_NAME)

	# draw status table
	draw_status(stdscr, 7, 8)

	# list options
	stdscr.addstr(22,8, "| c - control this ship                 |")
	stdscr.addstr(23,8, "| h - help                              |")
	stdscr.addstr(24,8, "| q - quit                              |")
	stdscr.addstr(25,8, "|---------------------------------------|")
	stdscr.refresh()

def draw_control(stdscr):
	stdscr.clear()
	curses.curs_set(False)

	# setup ship info
	stdscr.bkgd(0, curses.color_pair(1))
	stdscr.addstr(3,8,  "|---------------------------------------|")
	stdscr.addstr(4,8,  "| ShipManager v1.1 |                    |")
	stdscr.addstr(5,8,  "|---------------------------------------|")
	stdscr.addstr(6,8,  "| Ship:                                 |")
	stdscr.addstr(6,16, PLC_NAME)

	# draw status table
	draw_status(stdscr, 7, 8)

	# list options
	stdscr.addstr(22,8, "| + Throttle++      | - Throttle--      |")
	stdscr.addstr(23,8, "| a Anchor Toggle   | h Update Heading  |")
	stdscr.addstr(24,8, "| x Update X Dest   | y Update Y Dest   |")
	stdscr.addstr(25,8, "| p Autopilot Toggle| u Update Password |")
	stdscr.addstr(26,8, "| r return to prev  |                   |")
	stdscr.addstr(27,8, "|---------------------------------------|")
	stdscr.refresh()

def handle_help(stdscr):
	stdscr.clear()
	stdscr.bkgd(0, curses.color_pair(1))
	stdscr.addstr(3,8,  "*****************************************")
	stdscr.addstr(4,8,  "* ShipManager v1.1 Help                 *")
	stdscr.addstr(5,8,  "*****************************************")
	stdscr.addstr(6,8,  "* c - control a ship that was listed    *")
	stdscr.addstr(7,8,  "* h - show this help window             *")
	stdscr.addstr(8,8,  "* p - password recovery (not in prod)   *")
	stdscr.addstr(9,8,  "* r - return to main menu               *")
	stdscr.addstr(10,8, "*****************************************")
	stdscr.refresh()
	while 1:
		c = stdscr.getch()
		if c == ord('r'):
			break

def get_pwstring(stdscr):
	rstr = ""
	curses.curs_set(True)
	while 1:
			c = stdscr.getch()
			if c not in range(256):
				continue
			if c == ord('\n'):
				break
			else:
				rstr += chr(c)
			stdscr.addch(ord('*'))
	curses.curs_set(False)
	return rstr

def check_priv(stdscr):
	# load passwd hash
	vpass = open('passwd', 'r').read().strip()

	# have user enter password
	stdscr.clear()
	stdscr.bkgd(0, curses.color_pair(4))
	stdscr.addstr(3,8, "ADMIN PRIVILEGE REQUIRED FOR DIRECT SHIP CTRL")
	stdscr.addstr(5,8, "Password: ")
	stdscr.refresh()
	mypass = get_pwstring(stdscr)

	# on enter compare passwords
	grantaccess = False
	if md5(mypass).hexdigest() == vpass: # this is easier than using bash...update other l8r
		grantaccess = True
	stdscr.clear()
	if grantaccess:
		stdscr.bkgd(0, curses.color_pair(2))
		stdscr.addstr(3,8, "ACCESS GRANTED")
		stdscr.addstr(5,8, "Loading...")
		stdscr.refresh()
		for i in xrange(2, -1, -1):
			stdscr.addch(5, 19, i + ord('0'))
			stdscr.refresh()
			sleep(1)
	else:
		stdscr.bkgd(0, curses.color_pair(4))
		stdscr.addstr(3,8, "ACCESS DENIED")
		stdscr.refresh()
		sleep(2)
	return grantaccess

def input_box(title, prompt):
	win = curses.newwin(2, 20, 7, 8)
	title = title[:20]
	prompt = prompt[:10] + ": "
	win.bkgd(0, curses.color_pair(3))
	win.addstr(0, 0, title)
	win.addstr(1, 0, prompt)
	ibr = ""
	while 1:
		c = win.getch()
		if c not in range(256):
			continue
		if c == ord('\n'):
			break
		ibr += chr(c)
		win.addch(c)
	try:
		fr = int(ibr) % 1000
	except:
		fr = 0
	if fr < 0: fr = 0
	return fr

def handle_ctrl(stdscr):
	# get admin creds, validate
	if not check_priv(stdscr):
		return
	# Load window
	draw_control(stdscr)
	while 1:
		c = stdscr.getch()
		if c == ord('+'):
			write_register("hr", PLC_HR_THROTTLE, read_register("hr", PLC_HR_THROTTLE)+1)
		elif c == ord('-'):
			tmp = read_register("hr", PLC_HR_THROTTLE)
			if tmp == 0:
				continue
			else:
				write_register("hr", PLC_HR_THROTTLE, tmp-1)
		elif c == ord('a'):
			write_register("co", PLC_CO_ANCHOR, not read_register("co", PLC_CO_ANCHOR))
		elif c == ord('p'):
			write_register("co", PLC_CO_AUTOPILOT, not read_register("co", PLC_CO_AUTOPILOT))
		elif c == ord('r'):
			break
		elif c in [ord('h'), ord('x'), ord('y'), ord('u')]: # special case cuz redraw must follow
			if c == ord('h'):
				write_register("hr", PLC_HR_HEADING, input_box("Update Heading", "Heading"))
			elif c == ord('x'):
				write_register("hr", PLC_HR_DESTX, input_box("Update Destination", "X"))
			elif c == ord('y'):
				write_register("hr", PLC_HR_DESTY, input_box("Update Destination", "Y"))
			elif c == ord('u'):
				handle_pwrc(stdscr)
			draw_control(stdscr)
			update_status(stdscr, 7, 8)
		else:
			update_status(stdscr, 7,8)
			continue

def handle_pwrc(stdscr):
	stdscr.clear()
	stdscr.bkgd(0, curses.color_pair(1))

	# print current password on screen
	vpass = open('passwd', 'r').read().strip()
	stdscr.addstr(3,8, "Current passwd hash: ")
	stdscr.addstr(vpass)

	# # get old password
	# stdscr.addstr(5,8, "Enter old passwd: ")
	# op = get_pwstring(stdscr)

	# if md5(op).hexdigest() != vpass:
	# 	return

	# get new password
	stdscr.addstr(6,8, "Enter new passwd: ")
	np = get_pwstring(stdscr)
	stdscr.addstr(7,8, "  Enter it again: ")
	ap = get_pwstring(stdscr)

	# validate
	stdscr.clear()
	if np == ap:
		# open('passwd', 'w').write(md5(np).hexdigest()) # should use this instead of next two lines
		cmd = "echo -n " + np + " | md5sum | awk '{ print $1 }' > passwd"
		subprocess.call(cmd, shell=True)
		stdscr.bkgd(0, curses.color_pair(2))
		stdscr.addstr(3,8, "passwd updated successfully")
	else:
		stdscr.bkgd(0, curses.color_pair(4))
		stdscr.addstr(3,8, "passwords do not match")
	stdscr.refresh()
	sleep(2)

def app(stdscr):
	curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
	curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_GREEN)
	curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_MAGENTA)
	curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_RED)
	curses.halfdelay(5)
	draw_main(stdscr)
	while 1:
		c = stdscr.getch()
		if c == ord('c'):
			handle_ctrl(stdscr)
		elif c == ord('h'):
			handle_help(stdscr)
		elif c == ord('q'):
			break	
		else:
			update_status(stdscr, 7, 8)
			continue
		draw_main(stdscr)

# if plc details provided, rock on
if len(sys.argv) == 6:
	PLC_IP = sys.argv[1]
	PLC_PORT = int(sys.argv[2])
	r = requests.get('http://%s:%s/name/%s/' % (sys.argv[3], sys.argv[4], sys.argv[5]))
	if r:
		PLC_NAME = r.text
	else:
		PLC_NAME = "N/A"

# setup client
PLC_CLIENT = ModbusClient(PLC_IP, port=PLC_PORT)
tries = 0
while not PLC_CLIENT.connect() and tries < 10:
	print "Attempt %d: connecting to plc..." % (tries)
	tries += 1
	sleep(1)

# last try before fail
if not PLC_CLIENT.connect():
	print "ERROR: Unable to contact %s:%d via modbus/tcp" % (PLC_IP, PLC_PORT)
	sys.exit(1)

sys.exit(wrapper(app))
